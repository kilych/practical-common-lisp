;;; 4.3 S-expressions

23                                      ; the integer one hundred twenty-three
3/7                                     ; the ratio three-sevenths
1.0                                     ; the floating-point number one in default precision
1.0e0                                   ; another way to write the same floating-point number
1.0d0                                   ; the floating-point number one in "double" precision
1.0e-4                                  ; the floating-point equivalent to one-ten-thousandth
+42                                     ; the integer forty-two
-42                                     ; the integer negative forty-two
-1/4                                    ; the ratio negative one-quarter
-2/8                                    ; another way to write negative one-quarter
246/2                                   ; another way to write the integer one hundred twenty-three

"foo"                                   ; the string containing the characters f, o, and o.
"fo\o"                                  ; the same string
"fo\\o"                                 ; the string containing the characters f, o, \, and o.
"fo\"o"                                 ; the string containing the characters f, o, ", and o.
